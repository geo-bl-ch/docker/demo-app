# -*- coding: utf-8 -*-
import logging
import os

from datetime import datetime
from pyramid.config import Configurator
from pyramid_mako import add_mako_renderer


__VERSION__ = '1.0.0'


log = logging.getLogger('demo_app')


start_time = None


def get_start_time():
    return start_time


def main(global_config, **settings):  # pragma: no cover
    global start_time

    config = Configurator(settings=settings)

    log_level = '{0}'.format(os.environ.get('LOG_LEVEL')).lower()
    if log_level == 'error':
        log.setLevel(logging.ERROR)
    elif log_level == 'warning':
        log.setLevel(logging.WARNING)
    elif log_level == 'debug':
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    log.info('Initialize demo application, version {0}'.format(__VERSION__))

    config.include('demo_app')
    config.scan()

    start_time = datetime.now()

    log.info('Demo application is ready...')

    return config.make_wsgi_app()


def includeme(config):  # pragma: no cover
    config.include('pyramid_mako')

    # bind the mako renderer to other file extensions
    add_mako_renderer(config, ".html")

    config.include('demo_app.routes')
