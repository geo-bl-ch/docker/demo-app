# -*- coding: utf-8 -*-
import os


class Index(object):
    def __init__(self, request):
        self._request = request
        self._config = request.registry.settings

    def render(self):
        title = os.environ.get('DEMO_APP_TITLE') or 'Demo application'
        message = os.environ.get('DEMO_APP_MESSAGE') or 'This is a simple demo application using the Pyramid Web Framework.'
        background = os.environ.get('DEMO_APP_BACKGROUND') or 'white'
        return {
            'title': title,
            'message': message,
            'background': background
        }
