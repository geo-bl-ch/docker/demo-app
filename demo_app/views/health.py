# -*- coding: utf-8 -*-
import os

from datetime import datetime, timedelta
from pyramid.httpexceptions import HTTPOk, HTTPServerError
from demo_app import get_start_time

class Health(object):
    def __init__(self, request):
        self._request = request
        self._config = request.registry.settings

    def render(self):
        max_age = os.environ.get('DEMO_APP_MAX_AGE')
        if max_age and len(max_age) > 0:
            if datetime.now() > get_start_time() + timedelta(seconds=int(max_age)):
                return HTTPServerError()
        return HTTPOk()
