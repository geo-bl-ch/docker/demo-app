# -*- coding: utf-8 -*-

from demo_app.views.index import Index
from demo_app.views.health import Health


def includeme(config):
    
    config.add_route('health', '/health')
    config.add_view(Health,
                    attr='render',
                    route_name='health',
                    request_method='GET')

    config.add_route('index', '/')
    config.add_view(Index,
                    attr='render',
                    route_name='index',
                    renderer='demo_app:templates/index.html',
                    request_method='GET')

    config.commit()
