FROM registry.gitlab.com/geo-bl-ch/docker/python:alpine-3.18

USER 0

RUN apk update && \
    apk upgrade && \
    apk add py3-pip py3-setuptools py3-wheel

ADD --chown=1001:0 . /app

WORKDIR /app

RUN pip3 install -e .

EXPOSE 8080

CMD ["pserve", "app.ini"]

USER 1001