# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


requires = [
    'PasteScript',
    'pyramid',
    'pyramid_mako',
    'waitress'
    ]


setup(
    name='demo_app',
    version='1.0.0',
    description='Demo application for workshops.',
    long_description='Demo application for workshops.',
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 3",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author='Karsten Deininger',
    author_email='karsten.deininger@bl.ch',
    url='https://gitlab.com/geo-bl-ch/docker/demo-app',
    keywords='web pyramid pylons',
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    dependency_links=[],
    entry_points={
        'paste.app_factory': [
            'main = demo_app:main'
        ]
    }
)
